import { createHash } from "https://deno.land/std/hash/mod.ts";

export class Block {
    constructor(fileName, indexFile, indexAll, chunk, hashFile) {
        this.fileName = fileName; // Name der Datei
        this.indexFile = indexFile; // Index der Datei 0,1,2...
        this.indexAll = indexAll;  // Anzahl der Blöcke/Datei
        this.hashFile = hashFile;  // Hash der Datei
        this.hashPrevious = "";
        this.chunk = chunk;  // 500 Byte Stücke der Datei
        this.hash = this.calculateBlockHash();
    }
    calculateBlockHash() {
        const blockHash = createHash("sha256");
        blockHash.update(this.fileName + this.indexFile + this.indexAll + this.hashPrevious + JSON.stringify(this.chunk));
        return blockHash.toString();
    }
}

export class BlockChain {
    constructor() {
        this.chain = [];
        this.chain.push(this.createGenesisBlock());
    }
    createGenesisBlock() {
        return new Block("GenesisBlock", 0, 1, new Uint8Array(1), "0");
    }
    addBlock(newBlock) {
        newBlock.hashPrevious = this.chain[this.chain.length - 1].hash;
        newBlock.hash = newBlock.calculateBlockHash();
        this.chain.push(newBlock);
    }
    isBlockinChain(newBlock) {
        for (let i = 1; i < this.chain.length; i++) {
            if (newBlock.fileHash === this.chain[i].fileHash &&
                newBlock.indexFile === this.chain[i].indexFile &&
                newBlock.indexAll === this.chain[i].indexAll &&
                this.chain[i].hashPrevious === this.chain[i - 1].hash) {
                newBlock.hashPrevious = this.chain[i - 1].hash;
                newBlock.hash = newBlock.calculateBlockHash();
                if (newBlock.hash === this.chain[i].hash) {
                    return true;
                }
            }
        }
        return false;
    }
}
