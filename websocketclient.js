let websocket;
let arrayBuffer = [];
let chunkCount = 0;
let block = {
  method: "",
  fileName: "",
  fileDate: 0,
  indexFile: 0,
  indexAll: 0,
  fileHash: "",
  chunk: ""
}

const inputUri = document.getElementById('ws_uri');
let wsUri = inputUri.value;
const input = document.getElementById('inputfile');
const inputFileSize = document.getElementById('inputfilesize');
const inputChunkSize = document.getElementById('inputchunkanzahl');
const infoCheck = document.getElementById('infocheck');
let error = "";

function disconnectWebSocket() {
  if (typeof websocket !== "object" || websocket.readyState !== 1 ) {  //
    alert("Websocket is closed !");
    return;
  }
  websocket.close();
}

window.addEventListener('beforeunload', function(event) {
  websocket.close();
});

function readUri() {
  wsUri = inputUri.value;
}
function connectWebSocket() {
//  let wsUri = "ws://localhost:8080";
    websocket = new WebSocket(wsUri);
    websocket.onopen = function (event) {
      write("Websocket is open now.");
      wsStatus.innerHTML = "Websocket is OPEN";
      error = ""
    };
    websocket.onmessage = function (event) {
      //FinServer received:
      if (event.data.slice(0,23) === "****End Chunk Check****") {
        infoCheck.innerHTML = event.data.slice(23);
      }
      write(event.data);
    }
    websocket.onclose = function (event) {
      write(`Client closed: ${event.reason}`);
      wsStatus.innerHTML = "Websocket is CLOSED" + error;
    }
    websocket.onerror = function(event) {
      write(`Error, please check network or socket-server: ${event.reason}`);
      error = ": Error, please check network or socket-server";
      wsStatus.innerHTML = "Websocket is CLOSED" + error;
      return;
    }
}

function upload() {
  infoCheck.innerHTML = 'Check: ???';
  let bytes = 0;
  if (typeof websocket !== "object" || websocket.readyState !== 1 ) {  //
    alert("Websocket is not open, Please click 'Connect with Websocket' first");
    return;
  }
  if (!file) {
    alert("Please select a file before clicking 'Sent'");
    return;
  }
  chunkCount = 0;
  if (arrayBuffer.length === 0) {
    block.indexFile = 0;
    block.fileName = file.name;
    block.chunk = "";
    block.method = "Last Chunk Transfer";
    websocket.send(JSON.stringify(block));  
  } else {
    while (bytes < arrayBuffer.length) {
      block.indexFile = chunkCount;
      chunkCount++;
      block.fileName = file.name;
      block.chunk = arrayBuffer.slice(bytes,bytes+500).toString();
      block.method = "Chunk Transfer";
      bytes = bytes + 500;
      if (bytes >= arrayBuffer.length) {
        block.method = "Last Chunk Transfer";
      }
      websocket.send(JSON.stringify(block));
    }
  }
}

function write(msg) {
  const infobox = document.getElementById('infobox');
  const p = document.createElement('p');
  p.innerHTML = msg;
  infobox.appendChild(p);
}

function deleteInfoText() {
  const infobox = document.getElementById('infobox');
  while (infobox.lastElementChild) {
    infobox.removeChild(infobox.lastElementChild);
  }
}

function check() {
  let bytes = 0;
  if (typeof websocket !== "object" || websocket.readyState !== 1 ) {  //
    alert("Websocket is not open, Please click 'Connect with Websocket' first");
    return;
  }
  if (!file) {
    alert("Please select a file before clicking 'Check'");
    return;
  }
  chunkCount = 0;
  if (arrayBuffer.length === 0) {
    block.indexFile = 0;
    block.fileName = file.name;
    block.chunk = "";
    block.method = "Last Check";
    websocket.send(JSON.stringify(block));  
  } else {
    while (bytes < arrayBuffer.length) {
      block.indexFile = chunkCount;
      chunkCount++;
      block.fileName = file.name;
      block.chunk = arrayBuffer.slice(bytes,bytes+500).toString();
      block.method = "Chunk Check";
      bytes = bytes + 500;
      if (bytes >= arrayBuffer.length) {
        block.method = "Last Check";
      }
      websocket.send(JSON.stringify(block));
    }
  }
}
