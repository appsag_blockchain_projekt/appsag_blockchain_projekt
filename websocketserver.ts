// Copyright 2018-2021 the Deno authors. All rights reserved. MIT license.
import { Block, BlockChain} from "./block.js";
import { serve } from "https://deno.land/std@0.99.0/http/server.ts";
import {
  acceptWebSocket,
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket,
} from "https://deno.land/std@0.99.0/ws/mod.ts";

const blockChain = new BlockChain();

let fileName = "";
let fileInChain = true;
let counter = 0;

async function handleWs(sock: WebSocket) {
  console.log("socket connected!");
  await sock.send("Die Chain hat " + blockChain.chain.length + " Blöcke");
  try {
    for await (const ev of sock) {
      if (typeof ev === "string") {
        // text message
        if (blockChain.chain.length >= 200000) {
          await sock.send("Die Chain hat mit " + blockChain.chain.length + " Blöcken die maximale Grenze erreicht!!!");
          continue;
        }  
        const block = JSON.parse(ev);
        if (block.method === "Chunk Transfer" ||
            block.method === "Last Chunk Transfer"
        ) {
          const chunk = new Uint8Array(JSON.parse("[" + block.chunk + "]"));
          console.log(block);
          fileName = block.fileName;
          blockChain.addBlock(new Block(block.fileName,block.indexFile,block.indexAll,chunk,block.fileHash));
          await sock.send("Server received: " + chunk.length + " Bytes");
          if (block.method === "Last Chunk Transfer") {
            await sock.send(`----File ${fileName} in Chain gespeichert!----`);
            await sock.send("Die Chain hat " + blockChain.chain.length + " Blöcke");
            console.log(`----File ${fileName} in Chain gespeichert!----`);
            console.log("Die Chain hat " + blockChain.chain.length + " Blöcke");
          }
        } else if (block.method === "Chunk Check" ||
                   block.method === "Last Check") {
          const chunk = new Uint8Array(JSON.parse("[" + block.chunk + "]"));
          console.log(block);
          fileName = block.fileName;
          let inChain = blockChain.isBlockinChain(new Block(block.fileName,block.indexFile,block.indexAll,chunk,block.fileHash))
          fileInChain = fileInChain && inChain;
          if (inChain) {
            await sock.send("Chunk " + counter + " ist bereits in der Chain");
          } else {
            await sock.send("Chunk " + counter + " ist nicht in der Chain");
          }
          counter++;
          if (block.method === "Last Check" ) {
            if (fileInChain) {
              await sock.send("****End Chunk Check****Die Datei ist bereits in der Chain gespeichert");
            } else {
              await sock.send("****End Chunk Check****Die Datei ist nicht in der Chain gespeichert");
            }
            await sock.send("Die Chain hat " + blockChain.chain.length + " Blöcke");
            fileInChain = true;
            counter = 0;
          }
        } else {
          console.log(ev);
        }
      } else if (ev instanceof Uint8Array) {
        console.log(ev);
      } else if (isWebSocketPingEvent(ev)) {
        const [, body] = ev;
        // ping
        console.log("ws:Ping", body);
      } else if (isWebSocketCloseEvent(ev)) {
        // close
        const { code, reason } = ev;
        console.log("ws:Close", code, reason);
      }
    }
  } catch (err) {
    console.error(`failed to receive frame: ${err}`);

    if (!sock.isClosed) {
      await sock.close(1000).catch(console.error);
    }
  }
}

if (import.meta.main) {
  /** websocket echo server */
  const port = Deno.args[0] || "8080";
  console.log(`websocket server is running on :${port}`);
  for await (const req of serve(`:${port}`)) {
    const { conn, r: bufReader, w: bufWriter, headers } = req;
    acceptWebSocket({
      conn,
      bufReader,
      bufWriter,
      headers,
    })
      .then(handleWs)
      .catch(async (e) => {
        console.error(`failed to accept websocket: ${e}`);
        await req.respond({ status: 400 });
      });
  }
}
