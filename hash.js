// Set up handlers on load.

let file;

// rechnet den hash wenn eine datei über das fileinput ausgewählt wird
let fileinput = document.getElementById('inputfile');
fileinput.addEventListener('change', handleFileInputChange);

// rechnet den hash wenn eine datei in die drop area gezogen wird
const dropZone = document.getElementById('dropzone');
dropZone.addEventListener('dragover', handleDropAreaDragover);
dropZone.addEventListener('drop', handleDropAreaDrop);

// öffnet die file input prompt when the drop area is clicked
dropZone.addEventListener('click', handleDropAreaClick);

// Event handlers

function handleFileInputChange() {
  // Hash a local file when selected via file input.
  clearResult();
  let fileinput = this;
  file = fileinput.files[0];

  hashFile(file)
    .then(showHash)
    .catch(showHashError);
}

function handleDropAreaDragover(evt) {
  // Set up the drop area.
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy';
}

function handleDropAreaDrop(evt) {
  // Hash the file that is dropped into the drop area.
  evt.preventDefault();
  clearResult();
  file = evt.dataTransfer.files[0];
  fileinput.value = '';

  hashFile(file)
    .then(showHash)
    .catch(showHashError);
}

function handleDropAreaClick(evt) {
  // Show the file select dialog.
  evt.preventDefault();
  let doc = document.getElementById('inputfile').ownerDocument;
  let fileinputClick = doc.createEvent('MouseEvents');
  fileinputClick.initEvent('click', true, true);
  fileinput.dispatchEvent(fileinputClick, true);
}


function checkIfLocalCopy() {
  // Hide the description, including a recommendation to save if the app
  // has been loaded from a local file.
  if (/^file:\/\//.test(window.location.toString())) {
    document.getElementById('description').style.visibility = 'hidden';
  }
}

// Utilities

function bufferToHex(buffer) {
  // Convert a buffer into a hexadecimal string.
  // From https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
  let hexCodes = [];
  let view = new DataView(buffer);

  for (let i = 0; i < view.byteLength; i += 4) {
    // Using getUint32 reduces the number of iterations needed (we process
    // 4 bytes each time).
    let value = view.getUint32(i);
    // toString(16) will give the hex representation of the number without
    // padding
    let stringValue = value.toString(16);
    // We use concatenation and slice for padding.
    let padding = '00000000';
    let paddedValue = (padding + stringValue).slice(-padding.length);
    hexCodes.push(paddedValue);
  }

  return hexCodes.join("");
}

function bufferToBase64(buffer) {
  // Convert a buffer into a base64 string.
  let str = '';
  let bytes = new Uint8Array(buffer);
  let len = bytes.byteLength;

  for (let i = 0; i < len; i++) {
    str += String.fromCharCode(bytes[i]);
  }

  return window.btoa(str);
}

function hashFile(file) {
  // Hash a File object.
  // Returns a Promise of a successful hash.
  return new Promise(function (resolve, reject) {

    let fileReader = new FileReader();
    inputFileSize.innerHTML = "Datei: " + file.name + " Dateigröße: " + file.size + " bytes";
    inputChunkSize.innerHTML = "Die Datei wird voraussichtlich in: " + Math.ceil(file.size/500) + " chunks aufgeteilt werden";
    block.fileDate = file.lastModified;
    if (file.size === 0) {
      block.indexAll = 1;
    } else {
      block.indexAll = Math.ceil(file.size/500);
    }
    fileReader.addEventListener('load', function () {
      arrayBuffer = new Uint8Array(this.result);  
      crypto.subtle.digest("SHA-256", this.result)
        .then(resolve)
        .catch(reject);
    });
    fileReader.readAsArrayBuffer(file);
  });
}


// DOM mutation

function showHash(hash) {
  // Display the hash as a hex string.
  let sha256 = bufferToHex(hash);
  block.fileHash = sha256;
  document.getElementById('result').innerHTML =
    '<h2>SHA-256 Hash</h2><p>' + sha256;
}

function clearResult() {
  document.getElementById('result').innerHTML = '';
  document.getElementById('infocheck').innerHTML = 'Check: ???';
}

function showHashError(err) {
  // Display a generic hash error.
  document.getElementById('result').innerHTML =
    '<h2>' + gettext('error-header') + '</h2>' +
    '<p>' + gettext('error-hash');
}

