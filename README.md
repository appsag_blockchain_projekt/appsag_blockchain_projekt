# appsag_blockchain_projekt

hier versucht die studieren ab 50 selbstorganisierte arbeitsgemeinschaft appetit auf apps, das projekt der netzwerkprogrammierungsübung in javascript bzw. typescript mit- bzw. nachzubauen ;-)

wir sind uns bewusst, dass die umsetzung noch lange nicht wirklich fertig ist, möchten aber zu der letzten session der übungsgruppe schon mal was kleines zu der unterhaltung der betreuer beitragen ;-)

da die appsag als javascript laufzeit nicht node sondern deno benutzt, braucht man um mit dem projekt rumzuspielen deno.

deno besteht aus einer einzigen datei (inklusive linter, formatter und tester)

in den zipdateien die @ https://github.com/denoland/deno/releases/tag/v1.12.0 unter assets zu finden sind, versteckt sich genau eine datei, die sich in usr/local/bin sehr wohl fühlt

es ist also kein homebrew oder so notwendig

# anleitung

- den projektordner sehr gerne in vs code (weil eingebauter zugang zum terminal) öffnen
- im terminal die websocketserver.ts mit "deno run -A websocketserver.ts" anwerfen
- danach im browser die index.html öffnen
- mit einem click mit dem websocket verbinden und mal ein paar dateien ausprobieren

gerne auch:

- mehrere clients (browserfenster) mit dem websocketserver verbinden und richtig große und kleinere dateien in die blockchain packen
- unter https://appsag.uber.space/ ausprobieren, ob wir herausgefunden haben, wie die ws url zum websocketserver im richtigen internetz lauten muss ;-))

viel spaß
